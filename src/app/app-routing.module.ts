import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponentComponent} from './home-component/home-component.component';
import {PostDetailComponent} from './home-component/post-detail/post-detail.component';
import {LoginPageComponent} from './log-in/login-page/login-page.component'
import {RegisterPageComponent} from './log-in/register-page/register-page.component'
import {PostIndexComponent} from './post/post-index/post-index.component'
import {DetailComponent} from './post/detail/detail.component'
import {AddPostComponent} from './post/add-post/add-post.component'
import {EditPostComponent} from './post/edit-post/edit-post.component'

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponentComponent,
  },
  {
    path: 'detail/:id',
    component: PostDetailComponent ,
  },
  {
    path: 'login',
    component: LoginPageComponent ,
  },
  {
    path: 'register',
    component: RegisterPageComponent,
  },
  {
    path: 'post/index',
    component: PostIndexComponent,
  },
  {
    path: 'post/create',
    component: AddPostComponent,
  },
  {
    path: 'post/detail/:id',
    component: DetailComponent,
  },
  {
    path: 'post/edit/:id',
    component: EditPostComponent,
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
