import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {urlLogin} from 'src/app/constant/api.const';
import {FormModel} from 'src/app/model/form-control';
import {  CookieService } from 'ngx-cookie-service'; 
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  err: string;
  logInForm: FormGroup;
  userModel;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private formModel : FormModel,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {
    const fb = new FormBuilder();
    this.logInForm = fb.group({
      userName: new FormControl(),
      passWord: new FormControl(),
    });
    this.formModel.createForm();
    this.userModel = this.formModel.getFormModel().controls['user'];
  }

  logIn() {
    const account = {
      Username: this.logInForm.controls.userName.value,
      Password: this.logInForm.controls.passWord.value,
    }
    this.http.post(urlLogin, account).subscribe(res => {
      if (res && res['token']) {
        this.userModel.controls.userName.setValue(account.Username);
        this.cookieService.set('username',account.Username);
        this.userModel.controls.name.setValue(res['firstName'] + res['lastName']);
        this.cookieService.set('name', res['firstName'] + res['lastName']);
        this.userModel.controls.role.setValue(res['role']);
        this.cookieService.set('role', res['role']);
        this.cookieService.set('token', res['token']);
        this.router.navigate(['home', this.userModel.value]);
      } else {
        this.err = 'Please Re-Enter your Account';
      }
    })

  }
}
