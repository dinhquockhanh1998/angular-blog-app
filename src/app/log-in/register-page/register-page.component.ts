import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {urlRegister} from 'src/app/constant/api.const';
import {FormModel} from 'src/app/model/form-control';
import {  CookieService } from 'ngx-cookie-service'; 
import { ErrorStateMatcher } from '@angular/material/core';

// export class MyErrorStateMatcher implements ErrorStateMatcher {
//     isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//       const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
//       const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);
  
//       return (invalidCtrl || invalidParent);
//     }
//   }

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {
  err: string;
  registerForm : FormGroup;
  // matcher = new MyErrorStateMatcher();
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private formModel : FormModel,
    private cookieService : CookieService,
  ) { }
  

  ngOnInit(): void {
    const fb = new FormBuilder();
    this.registerForm = fb.group({
      userName: new FormControl('',Validators.required),
      passWord: new FormControl('',Validators.required),
      confirmPass: new FormControl(''),
      email: new FormControl('',Validators.required),
      firstName: new FormControl('',Validators.required),
      lastName: new FormControl('',Validators.required),

    });
  }
  register() {
    const account = {
      Username: this.registerForm.controls.userName.value,
      Password: this.registerForm.controls.passWord.value,
      ConfirmPass: this.registerForm.controls.confirmPass.value,
      Email: this.registerForm.controls.email.value,
      FirstName: this.registerForm.controls.firstName.value,
      LastName: this.registerForm.controls.lastName.value,
      Role: 'User',
    }
    
    if (account.Password === account.ConfirmPass){
      this.http.post(urlRegister, account).subscribe(res => {
        if (!res) {
          this.router.navigate(['login']);
        } else {
          return this.err = 'Please Re-Enter your Account';
        }
      })
    }
     else{
       this.err = 'ConfirmPassword field does not match'
     }
 
}

//   checkPass (group: FormGroup) { // here we have the 'passwords' group
//   let pass = group.get('passWord').value;
//   let confirmPass = group.get('confirmPass').value;

//   return pass === confirmPass ? null : { notSame: true }     
// }

}
