import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlGetListPost } from 'src/app/constant/api.const';
import {Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PostModel } from 'src/app/model/post.model';
import {CookieService } from 'ngx-cookie-service';  
import { Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {categoryList} from 'src/app/constant/categoryList.const';


@Component({
  selector: 'app-post-index',
  templateUrl: './post-index.component.html',
  styleUrls: ['./post-index.component.scss']
})
export class PostIndexComponent implements OnInit {
  displayedColumns: string[] = ['Id', 'CategoryId', 'Title','Published', 'AddedDate', 'AddedBy', 'ModifiedDate', 'ModifiedBy'];
  data = [];

  isLoadingResults = true;
  
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {
    this.getListPost();

    
  }


  getListPost() {
    this.http.get(urlGetListPost).subscribe((res: any) => {
      this.data = res;
      this.data.forEach((r) => {
        r.categoryId = categoryList.find((c) => c.CategoryId === r.categoryId).Name
      });
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
}
