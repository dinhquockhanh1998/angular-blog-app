import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlGetDetailPost, urlDeletePost } from 'src/app/constant/api.const';
import {Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PostModel } from 'src/app/model/post.model';
import {CookieService } from 'ngx-cookie-service';  
import { Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {categoryList} from 'src/app/constant/categoryList.const';




@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  post : PostModel = {
    id: '', 
    categoryId: '', 
    title: '', 
    shortDescription: '', 
    fullDescription: '', 
    published: null,
    addedDate: '', 
    addedBy: '', 
    modifiedDate: '', 
    modifiedBy: ''};
    token = this.cookieService.get('token');
  isLoadingResults = true;  
  
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {
    this.getDetailPost();
  }


  getDetailPost() {
    const id = this.route.snapshot.paramMap.get('id');
    this.http.get(urlGetDetailPost +id).subscribe((res : any) => {
      this.post = res;  
      console.log(this.post);
      this.isLoadingResults = false;
    })
  }

  deletePost(post){
    const serviceData = {
        id : post.id,
    };
    const options = {
      headers: new HttpHeaders({
        'Authorization' : "Bearer " +this.token,
        'Content-Type': 'application/json'
      }),
      body: serviceData
    }
    this.http.delete(urlDeletePost, options).subscribe(res => {
      this.isLoadingResults = false;
      this.router.navigate(['/post/index']);
    }, (err) => {
      console.log(err);
      this.isLoadingResults = false;
    }
  );
  }

}
