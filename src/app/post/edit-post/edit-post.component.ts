import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlGetDetailPost, urlEditPost } from 'src/app/constant/api.const';
import {Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PostModel } from 'src/app/model/post.model';
import {CookieService } from 'ngx-cookie-service';  
import { Observable } from 'rxjs';
import {categoryList} from 'src/app/constant/categoryList.const';
import { ErrorStateMatcher } from '@angular/material/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, FocusEvent, BlurEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
// import  {Adapter}  from 'src/app/model/ckEditorAdapter';
import  {MyUploadAdapter}  from 'src/app/model/ckEditorAdapter';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
  public Editor = ClassicEditor;
  public config = {
    extraPlugins: [ this.MyCustomUploadAdapterPlugin],
    placeholder: 'Type the content here!',
  };
  public onReady( editor ) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement(),
      
    );
    
  }
  err: string;
  postForm : FormGroup;
  categoryList = [1,2,3,4];
  publishList = [true, false];
  isLoadingResults = false;
  token = this.cookieService.get('token');
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {
    this.getDetailPost();
    const fb = new FormBuilder();
    this.postForm = fb.group({
      id: new FormControl(null),
      categoryId: new FormControl(null),
      title: new FormControl(null),
      shortDescription: new FormControl(null),
      fullDescription: new FormControl(null),
      published: new FormControl(null),
     });
  
  }

  getDetailPost() {
    const id = this.route.snapshot.paramMap.get('id');
    this.http.get(urlGetDetailPost +id).subscribe((data : any) => {
      this.postForm.setValue({
        id : id,
        categoryId : data.categoryId,
        title : data.title,
        shortDescription : data.shortDescription,
        fullDescription : data.fullDescription,
        published : data.published,
      });  
      })
  }

  editPost(){
    const Post = {
      Id:  +this.route.snapshot.paramMap.get('id'),
      CategoryId: this.postForm.controls.categoryId.value,
      Title: this.postForm.controls.title.value,
      ShortDescription: this.postForm.controls.shortDescription.value,
      FullDescription: this.postForm.controls.fullDescription.value,
      Published: this.postForm.controls.published.value,
      ModifiedBy: this.cookieService.get('name'),
    };
     const options = {
       headers: new HttpHeaders({
         'Authorization' : "Bearer " +this.token,
         'Content-Type' : 'application/json',
     }),
       body: Post
    };
    this.isLoadingResults = true;
    this.http.put(urlEditPost, Post, options).subscribe((res:any) =>{
      this.isLoadingResults = false;
      this.router.navigate(['/post/index']);
    },(err: any) => {
      console.log(err);
      this.isLoadingResults = false;
    }
  );
}

postDetail() {
  this.router.navigate(['/post/detail/', this.route.snapshot.paramMap.get('id')]);
}

MyCustomUploadAdapterPlugin( editor ) {
  editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
      // Configure the URL to the upload script in your back-end here!
      return new MyUploadAdapter( loader );
  };
}
}


