import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlAddPost } from 'src/app/constant/api.const';
import {Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PostModel } from 'src/app/model/post.model';
import {CookieService } from 'ngx-cookie-service';  
import { Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {categoryList} from 'src/app/constant/categoryList.const';
import { ErrorStateMatcher } from '@angular/material/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, FocusEvent, BlurEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
// import  {Adapter}  from 'src/app/model/ckEditorAdapter';
import  {MyUploadAdapter}  from 'src/app/model/ckEditorAdapter';

// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }


@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  public Editor = ClassicEditor;
  public config = {
    extraPlugins: [ this.MyCustomUploadAdapterPlugin],
    placeholder: 'Type the content here!',
  };
  public onReady( editor ) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement(),
      
    );
    
  }

  err: string;
  postForm : FormGroup;
  categoryList = [1,2,3,4];
  publishList = [true,false];
  isLoadingResults = false;
  // matcher = new MyErrorStateMatcher();
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {
    const fb = new FormBuilder();
    this.postForm = fb.group({
      categoryId: new FormControl(),
      title: new FormControl(),
      shortDescription: new FormControl(),
      fullDescription: new FormControl(),
      published: new FormControl(),
     });
  }
addPost(){
  const Post = {
    CategoryId: this.postForm.controls.categoryId.value,
    Title: this.postForm.controls.title.value,
    ShortDescription: this.postForm.controls.shortDescription.value,
    FullDescription: this.postForm.controls.fullDescription.value,
    Published: false,
    AddedBy: this.cookieService.get('name'),
    ModifiedBy: this.cookieService.get('name'),
  }
  
  this.http.post(urlAddPost,Post).subscribe((res:any) => {
    this.isLoadingResults = false;
    this.router.navigate(['post/index']);
  }, (err: any) => {
    console.log(err);
    this.isLoadingResults = false;
  });
}

// customAdapterPlugin(editor) {
//   editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
//     return new Adapter(loader, editor.config);
//   };
// }

 MyCustomUploadAdapterPlugin( editor ) {
   editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
       // Configure the URL to the upload script in your back-end here!
       return new MyUploadAdapter( loader );
   };
 }
}

// class UploadAdapterPlugin  {
// 	constructor( editor: any ) {
// 		const { httpClient } = editor.config.get( 'uploadAdapter' );
// 		editor.plugins.get( 'FileRepository' ).createUploadAdapter = loader => {
// 			// Modify for your endpoint URL.
// 			return new MyUploadAdapter( loader, httpClient, 'https://localhost:44383/Image' );
// 		};
// 	}
// }


