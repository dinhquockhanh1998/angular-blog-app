export interface PostModel {
    id: string;
    categoryId: string;
    title: string;
    shortDescription: string;
    fullDescription: string;
    published: boolean;
    addedDate: string;
    addedBy: string;
    modifiedDate: string;
    modifiedBy: string;
}