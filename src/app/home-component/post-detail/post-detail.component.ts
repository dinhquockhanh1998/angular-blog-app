import { Component, OnInit } from '@angular/core';
import { urlGetDetailPost, urlComment } from 'src/app/constant/api.const';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router, ActivatedRoute } from '@angular/router';
import { PostModel } from 'src/app/model/post.model';
import {  CookieService } from 'ngx-cookie-service'; 
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {
  err: string;
  postDetail;
  listComments;
  commentForm : FormGroup;
  cookieName = this.cookieService.get('name');
  token = this.cookieService.get('token');
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {
    this.getDetailPost();
    const fb = new FormBuilder();
    this.commentForm = fb.group({
      content: new FormControl(null),
     });
  }

  getDetailPost() {
      const id = this.route.snapshot.paramMap.get('id');
      this.http.get(urlGetDetailPost +id).subscribe((res) => {
        this.postDetail = res ;  
       
      })
      this.http.get(urlComment+id+'/comments').subscribe((res1) => {
        this.listComments = res1 ;  
      })
  }

  postComment(){
    const comment = {
      content: this.commentForm.controls.content.value,
      addedBy: this.cookieName,
    
  };
  const options = {
    headers: new HttpHeaders({
      'Authorization' : "Bearer " +this.token,
      'Content-Type' : 'application/json',
  }),
    body: comment
 };
 const id = this.route.snapshot.paramMap.get('id');
 this.http.post(urlComment+id+'/comments/PostComment', comment, options).subscribe((res:any) =>{
   this.commentForm.reset();
  this.getDetailPost();
  },(err: any) => {
  console.log(err);
}
);
  }

  logout(): void{
    this.cookieService.deleteAll();
    this.router.navigate(['/home']);
  }

}