import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlGetListPost } from 'src/app/constant/api.const';
import {Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PostModel } from 'src/app/model/post.model';
import {  CookieService } from 'ngx-cookie-service';  



@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.scss']
})
export class HomeComponentComponent implements OnInit {
  posts;
  showPosts;
  pageNumber = 3;
  pageIndex = 0;
  isShowNext = true;
  isShowPre = false;
  cookieName = this.cookieService.get('name');
   constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router : Router,
    private cookieService : CookieService,
  ) { }

  ngOnInit(): void {

    this.getListPost();
    
 
  }

  getListPost() {
    this.http.get(urlGetListPost).subscribe((res) => {
      this.posts = res ;
      this.posts = this.posts.filter((p) => p['published']);   
      this.showPosts = this.posts.slice(0, this.pageNumber);
   
    })
  }
  

  sortPage(isIncrease) {
    isIncrease ? this.pageIndex++ : this.pageIndex--;
    this.isShowPre = this.pageIndex !== 0;
    this.isShowNext = this.pageNumber * this.pageIndex +  this.pageNumber < this.posts.length;
    this.showPosts = this.posts.slice(this.pageNumber * this.pageIndex, this.pageNumber * this.pageIndex +  this.pageNumber)
  }

  logout(): void{
    this.cookieService.deleteAll();
    this.router.navigate(['/home']);
  }



}
