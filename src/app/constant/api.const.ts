export const urlGetListPost = 'https://localhost:44383/Post/GetAll';
export const urlGetDetailPost = 'https://localhost:44383/Post/Get/';
export const urlAddPost = 'https://localhost:44383/Post/Create';
export const urlEditPost = 'https://localhost:44383/Post/Put';
export const urlDeletePost = 'https://localhost:44383/Post/Delete'
export const urlLogin = 'https://localhost:44383/User/authenticate';
export const urlRegister = 'https://localhost:44383/User/Register';
export const urlComment = 'https://localhost:44383/Post/';