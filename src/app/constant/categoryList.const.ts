import { CategoryModel } from 'src/app/model/category.model';
import {  CookieService } from 'ngx-cookie-service';  
import { Observable } from 'rxjs';
export const categoryList: CategoryModel[]  = [
    {
        CategoryId: 1,
        Name: '.Net MVC Core',
        Description: '.Net MVC Core',
    },
    {
        CategoryId: 2,
        Name: 'EF Core',
        Description: 'EF Core',
    },
    {
        CategoryId: 3,
        Name: 'Dapper ORM',
        Description: 'Dapper ORM',
    },
    {
        CategoryId: 4,
        Name: 'NHibernate Framework',
        Description: 'NHibernate Framework',
    },
]